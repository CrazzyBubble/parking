﻿// TODO: implement enum VehicleType.
//       Items: PassengerCar, Truck, Bus, Motorcycle.

namespace CoolParking.BL.Models
{
    public enum VehicleType:ushort
    {
        Bus = 1,
        Motorcycle,
        PassengerCar,
        Truck
    }
}