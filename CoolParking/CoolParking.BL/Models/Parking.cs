﻿// TODO: implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.

using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public class Parking
    {
        private static Parking parking;
        private Parking()
        {
            Vehicles = new List<Vehicle>();
            Balance = Settings.Balance;
        }
        public static Parking getParking()
        {
            parking ??= new Parking();
            return parking;
        }
        public decimal Balance { set; get; }
        public List<Vehicle> Vehicles;
        public int VehicleCount => Vehicles.Count;
        public Vehicle getVehicleById(string vehicleId)
        {
            return Vehicles.Find(vehicle => vehicle.Id.Equals(vehicleId));
        }
    }
}