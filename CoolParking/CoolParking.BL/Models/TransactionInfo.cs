﻿// TODO: implement struct TransactionInfo.
//       Necessarily implement the Sum property (decimal) - is used in tests.
//       Other implementation details are up to you, they just have to meet the requirements of the homework.

using System;

namespace CoolParking.BL.Models
{
    public struct TransactionInfo
    {
        public decimal Sum { get; }
        public string VehicleID { get; }
        public string TypeTransaction { get; }
        public DateTime TimeTransaction { get; }
        public TransactionInfo(decimal sum, string typeTransaction, string vehicleID, DateTime time)
        {
            Sum = sum;
            VehicleID = vehicleID;
            TypeTransaction = typeTransaction;
            TimeTransaction = time;
        }
        public override string ToString()
        {
            return String.Format("{0:HH:mm:ss}",TimeTransaction) + $"\t{VehicleID} - {TypeTransaction}: {Sum}";
        }
    }
}