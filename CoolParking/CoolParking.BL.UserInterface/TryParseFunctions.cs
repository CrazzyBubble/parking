﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CoolParking.BL.UserInterface
{
    static class TryParseFunctions
    {
        public static decimal TryReadDecimal(string textValue)
        {
            decimal value;
            if (Decimal.TryParse(textValue.Trim(), out value))
                return value;
            throw new ArgumentException("Incorrect argument.");
        }

        public static double TryReadDouble(string textValue)
        {
            double value;
            if (Double.TryParse(textValue.Trim(), out value))
                return value;
            throw new ArgumentException("Incorrect argument.");
        }

        public static ushort TryReadUShort(string textValue)
        {
            ushort value;
            if (UInt16.TryParse(textValue.Trim(), out value))
                return value;
            throw new ArgumentException("Incorrect argument.");
        }
        public static decimal TryReadDecimalPositive(string textValue)
        {
            var value = TryReadDecimal(textValue);
            if (value <= 0)
                throw new ArgumentException("Incorrect argument. Must be > 0.");
            return value;
        }

        public static double TryReadDoublePositive(string textValue)
        {
            var value = TryReadDouble(textValue);
            if (value <= 0)
                throw new ArgumentException("Incorrect argument. Must be > 0.");
            return value;
        }

        public static ushort TryReadUShortPositive(string textValue)
        {
            var value = TryReadUShort(textValue);
            if (value <= 0)
                throw new ArgumentException("Incorrect argument. Must be > 0.");
            return value;
        }
    }
}
