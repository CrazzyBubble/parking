﻿using CoolParking.BL.Models;
using System;
using System.Text;

namespace CoolParking.BL.UserInterface
{
    static class ConsoleTextFormater
    {
        public static ushort StandartLeft { set; get; } = 8;
        public static ushort StandartTop { set; get; } = 2;

        public static string ReplaceTabToSpace(string text)
        {
            return new StringBuilder(text).Replace("\t", "     ").ToString();
        }
        public static void ShowMessage(string text, ConsoleColor color=ConsoleColor.White, bool newLine=true, int addLeft=0)
        {
            Console.ForegroundColor = color;
            Console.CursorLeft = StandartLeft+addLeft;
            Console.Write(text);
            if (newLine)
                Console.WriteLine();
        }
        public static string UserConsoleEnter(string textEnter, ConsoleColor color = ConsoleColor.White)
        {
            ShowMessage($"Please, enter {textEnter}: ", color, false);
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            return Console.ReadLine().Trim();
        }
        public static void UserAwaiting()
        {
            UserConsoleEnter("somethings to next", ConsoleColor.DarkGray);
        }
        public static void ShowErrorMessage(string text)
        {
            ShowMessage($"Error: {text}", ConsoleColor.Red);
        }

        public static void ShowGoodMessage(string text = "It's fine!")
        {
            ShowMessage(text, ConsoleColor.Green);
        }

        public static void ShowTopicMessage(string text)
        {
            ShowMessage(text, ConsoleColor.Cyan);
        }
        public static void ShowMenuContent(string topic, string[] contentLines)
        {
            /** Show MENU **/
            const int leftRightSpaces = 5;
            int menuLength = topic.Length;
            for (int i = 0; i < contentLines.Length; ++i)
            {
                contentLines[i] = ReplaceTabToSpace(contentLines[i]);
                if (contentLines[i].Length > menuLength)
                    menuLength = contentLines[i].Length;
            }
            menuLength += leftRightSpaces * 2 + 2;
            Console.WindowWidth = menuLength + StandartLeft * 2+5;
            var text = new StringBuilder();
            // Show top board
            for(int i = 0; i < StandartTop; ++i)
                Console.WriteLine();
            //Console.CursorTop = StandartTop;
            text.Append('-', menuLength);
            ShowMessage(text.ToString());
            text.Clear();
            // Show left/right board
            for(int i = 0; i < contentLines.Length+3; ++i)
            {
                ShowMessage("|", newLine: false);
                ShowMessage("|", addLeft: menuLength-1);
            }
            // Show content
            Console.CursorTop = StandartTop + 2;
            ShowMessage(topic, ConsoleColor.DarkCyan, addLeft: leftRightSpaces + 4);
            foreach (var line in contentLines)
                ShowMessage(line, ConsoleColor.Cyan, addLeft: leftRightSpaces+1);
            Console.WriteLine();
            // Show bottom board
            text.Append('-', menuLength);
            ShowMessage(text.ToString());
            Console.WriteLine();
        }
        public static string GetVehicleInfo(Vehicle veh)
        {
            return $"{veh.Id}\t- {veh.VehicleType}\t- {veh.Balance}";
        }
    }
}
